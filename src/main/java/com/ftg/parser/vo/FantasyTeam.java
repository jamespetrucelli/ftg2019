package com.ftg.parser.vo;

public class FantasyTeam {

	public String teamAbbrev;
	
	public String teamLocation;
	
	public String teamNickname;
	
	public double totalPoints;
	
	public double getTotalPoints() {
		return totalPoints;
	}



	public void setTotalPoints(double totalPoints) {
		this.totalPoints = totalPoints;
	}



	public double runs;
	
	public double homeRuns;

	public double totalBases;
	
	public double rbi;
	
	public double kBatting;
	
	public double sbn;
	
	public double obp;
	
	public double kPitching;
	
	public double qs;
	
	public double win;
	
	public double loss;
	
	public double save;
	
	public double era;
	
	public double whip;
	
	
	@Override
	public String toString() {
		StringBuilder toStringBuilder = new StringBuilder();
		toStringBuilder.append(teamLocation + " " + teamNickname + "\t");
		toStringBuilder.append("totalPoints:" + totalPoints + "\t");
		toStringBuilder.append("runs:" + runs + "\t");
		toStringBuilder.append("homeRuns:" + homeRuns + "\t");
		toStringBuilder.append("totalBases:" + totalBases + "\t");
		toStringBuilder.append("rbis:" + rbi + "\t");
		toStringBuilder.append("kBatting:" + kBatting + "\t");
		toStringBuilder.append("sbn:" + sbn + "\t");
		toStringBuilder.append("obp:" + obp + "\t");
		toStringBuilder.append("kPitching:" + kPitching + "\t");
		toStringBuilder.append("qs:" + qs + "\t");
		toStringBuilder.append("win:" + win + "\t");
		toStringBuilder.append("loss:" + loss + "\t");
		toStringBuilder.append("save:" + save + "\t");
		toStringBuilder.append("era:" + era + "\t");
		toStringBuilder.append("whip:" + whip);
		
		
		return toStringBuilder.toString();
	}
	
	
	
	public void calcTotal() {
		this.totalPoints = runs + homeRuns + totalBases + rbi + kBatting + sbn + obp + kPitching + qs +  win + loss + save + era + whip;
	}
	
	
}
