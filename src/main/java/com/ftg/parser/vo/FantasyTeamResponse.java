package com.ftg.parser.vo;

import java.util.List;

public class FantasyTeamResponse {

	private List<FantasyTeam> teamList;

	public List<FantasyTeam> getTeamList() {
		return teamList;
	}

	public void setTeamList(List<FantasyTeam> teamList) {
		this.teamList = teamList;
	}
	
	
	
}
