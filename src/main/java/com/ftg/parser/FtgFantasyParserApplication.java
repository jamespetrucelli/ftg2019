package com.ftg.parser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import com.ftg.parser.rest.ESPNTeam;
import com.ftg.parser.rest.Response;
import com.ftg.parser.vo.FantasyTeam;

@SpringBootApplication
public class FtgFantasyParserApplication {

	private String ascendingStats[] = {"27","54","47","41"};
	
	public static void main(String[] args) {
		SpringApplication.run(FtgFantasyParserApplication.class, args);
	}

	
	 @Bean
	   public RestTemplate getRestTemplate() {
	      return new RestTemplate();
	   }

	
	@Bean
	public CommandLineRunner run(RestTemplate restTemplate) throws Exception {
		return args -> {
//			LinkedHashMap<String,ArrayList> response = restTemplate.getForObject(
//					"http://fantasy.espn.com/apis/v3/games/flb/seasons/2019/segments/0/leagues/10050?view=mLiveScoring&view=mMatchupScore&view=mRoster&view=mSettings&view=mStandings&view=mStatus&view=mTeam&view=modular&view=mNav", LinkedHashMap.class);
		
			
			
			
//			
//			Response response = restTemplate.getForObject(
//                    "http://fantasy.espn.com/apis/v3/games/flb/seasons/2019/segments/0/leagues/10050?view=mLiveScoring&view=mMatchupScore&view=mRoster&view=mSettings&view=mStandings&view=mStatus&view=mTeam&view=modular&view=mNav",
//                     Response.class);
//			
//			rankAndOrderStats(response.getTeams());
//			
//	
//			
//			
//			
			
			
		};
		
		
		
		
	}
	
//	
//	
//	private List<FantasyTeam> rankAndOrderStats(ESPNTeam[] teams){
//		List<FantasyTeam> teamList = new ArrayList<FantasyTeam>();
//		HashMap<String,HashMap<String,Double>> pointTotals = new HashMap<String,HashMap<String,Double>>() ;		
//		HashMap<String,HashMap<String,Double>> pointAllocations = new HashMap<String,HashMap<String,Double>>() ;
//		HashMap<String,FantasyTeam> mappingMap = new HashMap<String,FantasyTeam>();
//		Arrays.stream(teams).forEach( e->mapStats(e, pointTotals));
//		
//		Arrays.stream(teams).forEach( e->buildTeams(e, teamList));
//		
//		
//		Iterator<Map.Entry<String,HashMap<String,Double>>> statIterator = pointTotals.entrySet().iterator();
//		while(statIterator.hasNext()) {
//			HashMap<Double, List<String>> tempMap = new HashMap<Double, List<String>>(); 
//			Map.Entry<String,HashMap<String,Double>>entry = statIterator.next();
//			entry.getValue().entrySet().stream().forEach(e-> allocateStat(e,tempMap));
//			
//			
//			
//			
//			
//			if(Arrays.asList(ascendingStats).contains(entry.getKey())){
//				tempMap.entrySet().stream().sorted(Map.Entry.comparingByKey(Comparator.reverseOrder()))
//						.forEachOrdered(e-> computePoints(entry.getKey(),e,pointAllocations));
//				
//			}else {
//			
//				tempMap.entrySet().stream().sorted(Map.Entry.comparingByKey(Comparator.naturalOrder()))
//				.forEachOrdered(e-> computePoints(entry.getKey(),e,pointAllocations));
//					
//			}
//			
//
//		}
//		
//		
//		teamList.stream().forEach( e->populateTeamInfo(e, pointAllocations));
//		
//	
//		
//		
//	}
//	
//	
//	
//	
//	
//	private void populateTeamInfo(FantasyTeam team,HashMap<String,HashMap<String,Double>> pointMap) {
//	
//		
//		team.runs = pointMap.get("20").get(team.teamAbbrev);
//		team.homeRuns = pointMap.get("5").get(team.teamAbbrev);
//		team.totalBases = pointMap.get("8").get(team.teamAbbrev);
//		team.rbi = pointMap.get("21").get(team.teamAbbrev);
//		team.kBatting = pointMap.get("27").get(team.teamAbbrev);
//		team.sbn = pointMap.get("25").get(team.teamAbbrev);
//		team.obp = pointMap.get("17").get(team.teamAbbrev);
//		team.kPitching = pointMap.get("48").get(team.teamAbbrev);
//		team.qs = pointMap.get("63").get(team.teamAbbrev);
//		team.win = pointMap.get("53").get(team.teamAbbrev);
//		team.loss = pointMap.get("54").get(team.teamAbbrev);
//		team.save = pointMap.get("57").get(team.teamAbbrev);
//		team.era = pointMap.get("47").get(team.teamAbbrev);
//		team.whip = pointMap.get("41").get(team.teamAbbrev);
//		team.calcTotal();
//	
//}
//	
//	
//	private void buildTeams(ESPNTeam team, List<FantasyTeam> teamMap) {
//		
//			FantasyTeam fantasyTeam = new FantasyTeam();
//			fantasyTeam.teamAbbrev = team.getAbbrev();
//			fantasyTeam.teamLocation = team.getLocation();
//			fantasyTeam.teamNickname = team.getNickname();
//			teamMap.add(fantasyTeam);
//			
//		
//		
//		
//		
//	}
//	
//	
//	private void computePoints(String pointCd,Map.Entry<Double, List<String>> entry, HashMap<String,HashMap<String,Double>> pointMap) {
//		if(!pointMap.containsKey(pointCd)) {
//			pointMap.put(pointCd, new HashMap<String,Double>());
//		}
//		 double points = pointMap.get(pointCd).entrySet().size() +1;
//		 for(int i = 1; i < entry.getValue().size(); i++){
//			 points = points +( points +1);
//		 }
//		 points = points/entry.getValue().size();
//		 
//		 for(int i = 0; i < entry.getValue().size(); i++){
//			 pointMap.get(pointCd).put(entry.getValue().get(i), points);
//		 }
//		
//
//		 
//		 
//		
//	}
//	
//	private void allocateStat(Map.Entry<String,Double> entry,HashMap<Double, List<String>> tempMap) {
//		if(!tempMap.containsKey(entry.getValue())) {
//			tempMap.put(entry.getValue(), new ArrayList<String>());
//		}
//		tempMap.get(entry.getValue()).add(entry.getKey());
//		
//		
//	}
//
//	
//	
//	private void mapStats(ESPNTeam team,  HashMap<String,HashMap<String,Double>> resultMap) {
//		
//		Iterator<Map.Entry<String,Double>> statIterator = team.getValuesByStat().entrySet().iterator();
//		while(statIterator.hasNext()) {
//			Map.Entry<String, Double> entry = statIterator.next();
//			if(!resultMap.containsKey(entry.getKey())) {
//				resultMap.put(entry.getKey(), new HashMap<String,Double>());
//			}
//			resultMap.get(entry.getKey()).put(team.getAbbrev(), entry.getValue());
//			
//		}
//		
//		
//		
//
//	
//	}
//	
//	
//	
//	private FantasyTeam populateTeam(LinkedHashMap<String,Double> reqMap) {
//
//		
//		
//				FantasyTeam team = new FantasyTeam();
////		team.runs = reqMap.get("20");
////		team.homeRuns = reqMap.get("5");
////		team.totalBases = reqMap.get("8");
////		team.rbi = reqMap.get("21");
////		team.kBatting = reqMap.get("27");
////		team.sbn = reqMap.get("25");
////		team.obp = reqMap.get("17");
////		team.kPitching = reqMap.get("48");
////		team.qs = reqMap.get("63");
////		team.win = reqMap.get("53");
////		team.loss = reqMap.get("54");
////		team.save = reqMap.get("57");
////		team.era = reqMap.get("47");
////		team.whip = reqMap.get("41");
//		
//		return  team;
//	}
//	
}
