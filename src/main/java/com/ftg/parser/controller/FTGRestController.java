package com.ftg.parser.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ftg.parser.service.FTGParseService;
import com.ftg.parser.vo.FantasyTeam;

@RestController
public class FTGRestController {

	@Autowired
	public FTGParseService ftgService;
	
	
    @RequestMapping("/ftg")
    public List<FantasyTeam> home() {
        return ftgService.parseESPN();
    }
	
}
