package com.ftg.parser.rest;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown = true)
public class Response {

	
	private ESPNTeam[] teams;

	public ESPNTeam[] getTeams() {
		return teams;
	}

	public void setTeams(ESPNTeam[] teams) {
		this.teams = teams;
	}
	
	
	
	
}
