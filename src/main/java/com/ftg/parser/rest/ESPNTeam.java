package com.ftg.parser.rest;

import java.util.LinkedHashMap;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ESPNTeam {

	private String abbrev;
	
	private String location;
	
	private String nickname;
	
	private LinkedHashMap<String,Double> valuesByStat;

	public String getAbbrev() {
		return abbrev;
	}

	public void setAbbrev(String abbrev) {
		this.abbrev = abbrev;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public LinkedHashMap<String, Double> getValuesByStat() {
		return valuesByStat;
	}

	public void setValuesByStat(LinkedHashMap<String, Double> valuesByStat) {
		this.valuesByStat = valuesByStat;
	}


	
	
}
